const documentReady = (callback) => {
  if (document.readyState !== "loading") callback();
  else document.addEventListener("DOMContentLoaded", callback);
};

const windowWidth = window.innerWidth;
const windowHeight = window.innerHeight;
const header = document.querySelector("header");

// header anchor tags
const headerAnchors = () => {
  let pageDirection = "";
  let thisElement;
  let timeout;
  const navLinks = header.querySelectorAll("nav li a");

  navLinks.forEach((link) => {
    link.addEventListener("click", (event) => {
      event.preventDefault();
      document.querySelector(".cube").classList.remove(`reverse-${pageDirection}`);
      thisElement = event.target;
      pageDirection = thisElement.dataset.direction;
      reverseDirection = thisElement.dataset.reverseDirection;
      thisElement.parentNode.classList.add("active");
      Array.from(thisElement.parentNode.parentNode.children)
        .filter((child) => child !== thisElement.parentNode)
        .forEach((sibling) => sibling.classList.remove("active"));
      document.querySelector(".cube").classList.add(`reverse-${pageDirection}`);
      header.classList.add("go-out");
      document.querySelector("#wrap").classList.add("active");
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        header.classList.remove("go-out");
        document.querySelector("#wrap").classList.remove("active");
      }, 1000);
    });
  });
};
documentReady(headerAnchors);

window.addEventListener("resize", () => {
  const windowWidth = window.innerWidth;
  const windowHeight = window.innerHeight;
  // Functions
});

